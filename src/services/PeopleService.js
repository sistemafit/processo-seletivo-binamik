import Api from './api'

const getAllPaged = ({page = 1, search = ''}) => Api.get(`/people/?page=${page}&search=${search}`)

export {
  getAllPaged
}