import { Fragment, useEffect, useState, useCallback } from 'react';
import styled from 'styled-components'
import { ButtonOnlyIcon, Content, Header, Input, Loading, Table, Title } from 'components'
import { PeopleService } from 'services';
import { arrow } from 'images'

const getUrlParams = (url) => {
  return url
    ? url.split('?')[1].split('&')
      .reduce((actual, current) => {
        const splited = current.split('=')
        return ({ ...actual, [splited[0]]: splited[1] })
      }, {})
    : undefined;
}

const AlignCenter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const App = () => {
  const [search, setSearch] = useState('')
  const [loading, setLoading] = useState(false)
  const [people, setPeople] = useState([])
  const [total, setTotal] = useState(0)
  const [pagination, setPagination] = useState({ current: 1, next: 2, previous: 0 })

  const loadPeople = useCallback((param = { page: 1, search: '' }) => {
    setLoading(true)
    PeopleService.getAllPaged(param)
      .then(({ data }) => {
        setPeople(data.results)
        setTotal(data.count)

        let current = 0;
        let previous = 0;
        let next = 0;
        const nextUrlParams = getUrlParams(data.next)
        if (nextUrlParams) {
          next = Number(nextUrlParams.page)
          current = next - 1
        }
        const previousUrlParams = getUrlParams(data.previous)
        if (previousUrlParams) {
          previous = Number(previousUrlParams.page)
          current = previous + 1
        }
        setPagination({ current, previous, next })
      })
      .catch(() => {
        setPeople([])
        setTotal(0)
        setPagination({ current: 1, next: 2, previous: 0 })
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  const onChange = (event) => {
    const { value } = event.target;
    setSearch(value)
  }

  const onKeyDown = (event) => {
    if (event.key === 'Enter') {
      loadPeople({ page: 1, search })
    }
  }
  
  useEffect(() => {
    loadPeople()
  }, [loadPeople])

  return (
    <Fragment>
      <Header />
      <Content>
        <Content center>
          <Title>Find your character</Title>
          <AlignCenter>
            <Input name="search" aria-label="input-search" value={search} onChange={onChange} onKeyDown={onKeyDown} placeholder="Search" />
            <ButtonOnlyIcon onClick={() => loadPeople({ page: 1, search })} src={arrow} aria-label="button-search"/>
          </AlignCenter>
        </Content>
        {loading ? (
          <Loading show={loading} />
        ) : (
          <Table
            previous={pagination.previous}
            current={pagination.current}
            next={pagination.next}
            total={total}
            search={search}
            updateByPage={loadPeople}
          >
            <Fragment>
              <Table.Row header>
                <Table.DataHead>
                  Name
                </Table.DataHead>
                <Table.DataHead>
                  Gender
                </Table.DataHead>
                <Table.DataHead>
                  Height
                </Table.DataHead>
                <Table.DataHead>
                  Mass
                </Table.DataHead>
                <Table.DataHead>
                  Hair
                </Table.DataHead>
                <Table.DataHead>
                  Skin
                </Table.DataHead>
                <Table.DataHead>
                  Eyes
                </Table.DataHead>
              </Table.Row>
              {people.map(person => (
                <Table.Row key={person.url}>
                  <Table.Data name="Name">
                    {person.name}
                  </Table.Data>
                  <Table.Data name="Gender">
                    {person.gender}
                  </Table.Data>
                  <Table.Data name="Height">
                    {person.height}
                  </Table.Data>
                  <Table.Data name="Mass">
                    {person.mass}
                  </Table.Data>
                  <Table.Data name="Hair">
                    {person.hair_color}
                  </Table.Data>
                  <Table.Data name="Skin">
                    {person.skin_color}
                  </Table.Data>
                  <Table.Data name="Eyes">
                    {person.eye_color}
                  </Table.Data>
                </Table.Row>
              ))}
            </Fragment>
          </Table>
        )}
      </Content>
    </Fragment>
  );
}

export default App;
