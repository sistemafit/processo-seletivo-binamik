import { render, screen, fireEvent, waitFor, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import App from './App';

const PeopleService = require('services/PeopleService');

test('should render correctly', async() => {

  const getAllPaged = jest.spyOn(PeopleService, 'getAllPaged')
    .mockImplementation(() => Promise.resolve({
      data: {
        "count": 1, 
        "next": null, 
        "previous": null, 
        "results": [
          {
            "name": "Luke Skywalker", 
            "height": "172", 
            "mass": "77", 
            "hair_color": "blond", 
            "skin_color": "fair", 
            "eye_color": "blue", 
            "birth_year": "19BBY", 
            "gender": "male", 
            "homeworld": "http://swapi.dev/api/planets/1/", 
            "films": [
                "http://swapi.dev/api/films/1/", 
                "http://swapi.dev/api/films/2/", 
                "http://swapi.dev/api/films/3/", 
                "http://swapi.dev/api/films/6/"
            ], 
            "species": [], 
            "vehicles": [
                "http://swapi.dev/api/vehicles/14/", 
                "http://swapi.dev/api/vehicles/30/"
            ], 
            "starships": [
                "http://swapi.dev/api/starships/12/", 
                "http://swapi.dev/api/starships/22/"
            ], 
            "created": "2014-12-09T13:50:51.644000Z", 
            "edited": "2014-12-20T21:17:56.891000Z", 
            "url": "http://swapi.dev/api/people/1/"
          }
        ]
      }
    }));

  const component = render(<App />);
  const title = screen.getByText(/Find your character/i);
  expect(title).toBeInTheDocument();
  
  await waitFor(() => expect(getAllPaged).toHaveBeenCalledWith({ page: 1, search: ''}))

  const input = component.getByLabelText('input-search');
  fireEvent.change(input, { target: { value: 'luke' } });
  expect(input.value).toBe('luke');
  act(() => {
    userEvent.click(screen.getByTitle('Search'));
  })
  await waitFor(() => expect(getAllPaged).toHaveBeenCalledWith({ page: 1, search: 'luke' }))
  
  expect(screen.getByText(/Luke/i)).toBeInTheDocument();
});
