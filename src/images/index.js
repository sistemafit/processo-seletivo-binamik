import arrow from './right-arrow.svg'
import logo from './logo.png'

export {
	arrow,
	logo
}