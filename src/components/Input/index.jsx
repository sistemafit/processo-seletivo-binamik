import styled from 'styled-components'

const Input = styled.input`
	outline: none;
	font-size: 14px;
	padding: 5px 10px;
	width: 40vw;

	@media(max-width: 768px) {
		width: 100%;
	}
`

export default Input