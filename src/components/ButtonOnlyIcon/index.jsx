import PropTypes from 'prop-types'
import styled from 'styled-components'

const Button = styled.button`
	outline: none;
	border: none;
	background: none;
	cursor: pointer;
`
const ButtonOnlyIcon = ({ onClick, src }) => (
	<Button onClick={onClick} title="Search">
		<img width="40" src={src} alt="Search" />
	</Button>
)

ButtonOnlyIcon.propTypes = {
	onClick: PropTypes.func.isRequired,
	src: PropTypes.string.isRequired,
}

export default ButtonOnlyIcon