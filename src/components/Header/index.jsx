import styled from 'styled-components'
import { logo } from 'images'

const HeaderBox = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding: 1rem;
  & img {
    width: 150px;
    height: 75px;
  }
`

const Header = () => ( 
	<HeaderBox>
		<img src={logo} alt="Logo do Star Wars" />
	</HeaderBox>
)

export default Header