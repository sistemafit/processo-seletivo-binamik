import PropTypes from 'prop-types'
import styled from 'styled-components'

const Article = styled.article`
  padding: 1rem;
  ${props => props.center && 'text-align: center;'}
`

const Content = ({ children, center }) => ( 
	<Article center={center}>
    {children}
  </Article>
)

Content.propTypes = {
  center: PropTypes.bool,
}

Content.defaultProps = {
  center: false,
}

export default Content