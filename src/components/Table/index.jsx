import styled from 'styled-components'
import PropTypes from 'prop-types'

const Table = styled.div`
  color: white;
`
const TableRow = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 1fr 1fr 1fr 1fr 1fr;
  border: 1px solid #333;
  padding: 3px;
  align-items: center;
  
  &:nth-child(even) {
    background: rgba(0, 0, 0, 0.7);
  }
  &:hover {
    ${props => !props.header && 'background: rgba(255, 188, 0, 0.46);'}
  }

  @media(max-width: 768px) {
    grid-template-columns: auto;
    ${props => props.header && 'display: none;'}
  }
`
const TableData = styled.div`
  color: #ccc;
  @media(max-width: 768px) {
    &::before {
      display: inline-block;
      width: 85px;
      text-align: end;
      content: '${props => props.name}';
      color: #ccc;
      text-transform: uppercase;
      font-size: 10px;
      margin-right: 10px;
    }
  }
`
const TableColumnHead = styled.div`
  text-transform: uppercase;
  font-size: 14px;
  color: #999;
`
const TableFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 3px;
  border: 1px solid #333;
`
const Total = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-transform: uppercase;
  font-size: 12px;
  color: #ccc;
  opacity: 0.8;
`
const Button = styled.button`
  cursor: pointer;
  outline: none;
  border: none;
  border-radius: 3px;
  color: #ccc;
  background-color: #333;
  opacity: 0.5;
  &:hover {  
    opacity: 1;
  }
`
const ButtonPage = styled.button`
  cursor: ${props => props.actived ? '' : 'pointer'};  
  outline: none;
  border: none;
  border-radius: 50%;
  padding: 3px 7px;
  margin: 0 2px;
  color: #ccc;
  background-color: #333;
  opacity: ${props => props.actived ? '1' : '0.5'};
  &:hover {  
    opacity: 1;
  }
`

const MyTable = ({
  current,
  next,
  previous,
  updateByPage,
  total,
  children,
  search,
}) => ( 
	<Table>
    {children}
    <TableFooter>
      <Total>{total < 10 ? total : '10'}/{total} itens</Total>
      <div>
        {!!previous && (
          <>
            <Button onClick={() => updateByPage({ page: previous, search })}>Voltar</Button>
            <ButtonPage onClick={() => updateByPage({ page: previous, search })}>{previous}</ButtonPage>
          </>
        )}
        {!!current && <ButtonPage actived>{current}</ButtonPage>}
        {!!next && (
          <>
            <ButtonPage onClick={() => updateByPage({ page: next, search })}>{next}</ButtonPage>
            <Button onClick={() => updateByPage({ page: next, search })}>Próximo</Button>
          </>
        )}
      </div>
    </TableFooter>
  </Table>
)

MyTable.propTypes = {
  current: PropTypes.number.isRequired,
  previous: PropTypes.number.isRequired,
  next: PropTypes.number.isRequired,
  updateByPage: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  search: PropTypes.string,
}

MyTable.defaultProps = {
  search: '',
}

MyTable.Row = TableRow
MyTable.DataHead = TableColumnHead
MyTable.Data = TableData

export default MyTable