import styled, { keyframes } from 'styled-components'

const animation = keyframes`
	0% { transform: rotate(0deg); }
	100% { transform: rotate(360deg); }
`

const Loader = styled.div`
	border: 8px solid #f3f3f3;
	border-radius: 50%;
	border-top: 8px solid #ffc624;
	width: 30px;
	height: 30px;
	animation: ${animation} 2s linear infinite;
	position: fixed;
	left: 50vw;
`

const Loading = ({ show }) => (
	show ? <Loader /> : null
)

export default Loading